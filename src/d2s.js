const BitStream = require('bit-buffer').BitStream;

const STAT_DEFS = require('../resources/stats');
const CHARACTER_CLASSES = require('../resources/character_classes');
const STAT_BYTES_START = 765;

module.exports = class D2SReader {
	constructor (bytes) {
		this.bytes = bytes;

		this.char = {};
		this.flags = {};
		this.stats = {};
		
		this.readChar();
		this.readFlags();
		this.readStats();
	}

	getObject () {
		return {
			char: this.char,
			flags: this.flags,
			stats: this.stats
		};
	}

	getJSON () {
		return JSON.stringify(this.getObject(), null, 2);
	}

	isHardcore () {
		const hcBit = Buffer.alloc(1);
		this.bytes.copy(hcBit, 0, 36, 37);
		const bs = new BitStream(hcBit);
		bs.index = 2;

		return bs.readBits(1) == 1;
	}

	readChar () {
		this.char.name = this.bytes.toString('utf8', 0x14, 0x14+16).replace(/\u0000+/, '');
		this.char.class = CHARACTER_CLASSES[this.bytes[40]];
	}

	readFlags () {
		this.flags.hardcore = this.isHardcore();
	}

	readStats () {
		const statBytes = this.getStatBytes(this.bytes);
		const bs = new BitStream(statBytes);

		bs.index = 16;

		while (bs.bitsLeft >= 9) {
			let id = bs.readBits(9);

			if (id == 0x1ff) {
				break;
			}

			let stat = STAT_DEFS[id];

			if (!stat.CSvBits) {
				break;
			}

			this.stats[stat.Stat] = bs.readBits(stat.CSvBits, stat.Signed) >> (stat.ValShift || 0);
		}
	}

	getStatBytes (bytes) {
		const length = this.getItemListStart(bytes) - STAT_BYTES_START;
		
		const statBytes = Buffer.alloc(length);
		bytes.copy(statBytes, 0, STAT_BYTES_START, STAT_BYTES_START+length);

		return statBytes;
	}

	getItemListStart (bytes) {
		const J = 'J'.charCodeAt(0);
		const M = 'M'.charCodeAt(0);

		for (let i = STAT_BYTES_START; i < bytes.length-5; ++i) {
			if (bytes[i] == J && bytes[i+1] == M) {
				if (bytes[i+4] == J && bytes[i+5] == M) {
					return i;
				}
			}
		}
		
		return 0;
	}
}
